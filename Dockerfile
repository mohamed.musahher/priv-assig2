#Use the latest LTS version of Ubuntu as the base image   
FROM ubuntu:22.04
# Install the required dependencies

RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
        curl \
        ca-certificates
        
# Install the required dependencies
RUN apt-get upgrade -y

# Install the required dependencies
RUN apt install sudo -y

#Download the .NET SDK installer
RUN sudo apt-get update && \
  sudo apt-get install -y dotnet-sdk-6.0

#Download the .NET runtime installer
RUN sudo apt-get update && \
  sudo apt-get install -y aspnetcore-runtime-6.0


  #test trigger
