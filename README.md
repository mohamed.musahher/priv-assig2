# dotnetcore-docker



## Getting started

**requirements**:
_Docker_


1. in terminal you need first to build a image by typing `docker build --tag dotnet-docker .` here i named image for `dotnet-docker` you can name it what ever you want.

2. Now we need to run our image by typing in terminal `docker run -d -p 8081:80 --name test-docker  dotnet-docker` and here again you can choose which port should the container run from.

--------------------------------------------------------------------------------------------------------------------------------------
You can also Pull our image direct from docker hub all you need is to type in terminal `docker pull sinus707/docker-dotnet6`, and then you can run image through container.
